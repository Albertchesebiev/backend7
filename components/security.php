<?php
function unicode($text) {
  // применяем перед вставкой в HTML
  return htmlspecialchars($text, ENT_QUOTES, 'UTF-8');
}

function safeTags($text) {
  return strip_tags($text, '<b><a><i><q><sub><sup><u><br>');
}
