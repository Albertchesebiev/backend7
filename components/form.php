<head>
  <link rel="stylesheet" href="css/main.css">
  <link rel="stylesheet" href="css/user-form.css">
</head>

<body>
  <!-- MESSAGES -->
  <?php
  include('security.php');
  if (!empty($reg_data_msg)) {
    print('<div class="popup_message popup_left popup_top">' . safeTags($reg_data_msg) . '</div>');
  }
  if (!empty($popup_messages)) {
    foreach ($popup_messages as $key => $msg) {
      print('<div class="popup_message popup_right popup_top popup_timer">' . safeTags($msg) . '</div>');
    }

    // таймер для pop-up сообщений 
    print '<script>setTimeout(() => {
      document.querySelector(".popup_timer").remove();
    }, 3000);</script>';
  }
  ?>

  <form class="user-form" method="POST" action="">
    <h4>Name</h4>
    <input name="name" type="text" <?php if (in_array('name_error', $errors)) {
                                      print 'class="error_field"';
                                    } ?> value="<?php
                                                if (isset($values['name'])) {
                                                  print $values['name'];
                                                }
                                                ?>">
    <?php
    if (in_array("name_error", array_keys($messages))) {
      print '<div class="error_field_message">' . safeTags($messages["name_error"]) . '</div>';
    }
    ?>

    <h4>Email</h4>
    <input type="email" name="email" <?php if (in_array('email_error', $errors)) {
                                        print 'class="error_field"';
                                      } ?> value="<?php
                                                  if (isset($values['email'])) {
                                                    print $values['email'];
                                                  }
                                                  ?>">
    <?php
    if (in_array("email_error", array_keys($messages))) {
      print '<div class="error_field_message">' . safeTags($messages["email_error"]) . '</div>';
    }
    ?>


    <div class="row">

      <!-- BIRTHDAY -->
      <div class="wrap">
        <h4>Birthday</h4>
        <select name="birthday" <?php if (in_array('birthday_error', $errors)) {
                                  print 'class="error_field"';
                                } ?>>
          <?php for ($i = 1900; $i < 2020; $i++) { ?>
            <option value="<?= $i; ?>" <?php if (isset($values['birthday']) && $i == $values['birthday']) {
                                          print 'selected';
                                        } ?>><?= $i; ?></option>
          <?php } ?>
        </select>
        <?php
        if (in_array("birthday_error", array_keys($messages))) {
          print '<div class="error_field_message">' . safeTags($messages["birthday_error"]) . '</div>';
        }
        ?>
      </div>

      <!-- SEX -->
      <div class="wrap">
        <h4>Sex</h4>
        <label>
          Male
          <input type="radio" name="sex" value="m" <?php if (isset($values['sex']) && 'm' == $values['sex']) {
                                                      print 'checked';
                                                    } ?>>
        </label>
        <label>
          Female
          <input type="radio" name="sex" value="f" <?php if (isset($values['sex']) && 'f' == $values['sex']) {
                                                      print 'checked';
                                                    } ?>>
        </label>
        <?php
        if (in_array("sex_error", array_keys($messages))) {
          print '<div class="error_field_message">' . safeTags($messages["sex_error"]) . '</div>';
        }
        ?>
      </div>
    </div>

    <!-- LIMBS -->
    <h4 class="limbs_header">Num of limbs</h4>
    <div class="limbs_wrap">
      <?php for ($i = 0; $i <= 4; $i++) { ?>
        <label><?= $i ?><input type="radio" name="limbs" value="<?= $i ?>" <?php if (isset($values['limbs']) && $i == $values['limbs']) {
                                                                              print 'checked';
                                                                            } ?>></label>
      <?php } ?>

    </div>
    <?php
    if (in_array("limbs_error", array_keys($messages))) {
      print '<div class="error_field_message">' . safeTags($messages["limbs_error"]) . '</div>';
    }
    ?>

    <!-- SKILLS -->
    <h4>Skills</h4>
    <select name="skills[]" multiple <?php if (in_array('skills_error', $errors)) {
                                        print 'class="error_field"';
                                      } ?>>>
      <?php foreach ($skills_labels as $key => $value) { ?>
        <option value="<?= $key; ?>" <?php if (isset($values[$key])) {
                                        print 'selected';
                                      } ?>><?= $value; ?></option>
      <?php } ?>
    </select>
    <?php
    if (in_array("skills_error", array_keys($messages))) {
      print '<div class="error_field_message">' . safeTags($messages["skills_error"]) . '</div>';
    }
    ?>

    <!-- BIOGRAPHY -->
    <h4>Biography</h4>
    <textarea name="biography" cols="30" rows="5" placeholder="Biography have to include minimum 10 symbols" <?php if (in_array('biography_error', $errors)) {
                                                                                                                print 'class="error_field"';
                                                                                                              } ?>><?php
                                                                                                                    if (isset($values['biography'])) {
                                                                                                                      print $values['biography'];
                                                                                                                    }
                                                                                                                    ?></textarea>
    <?php
    if (in_array("biography_error", array_keys($messages))) {
      print '<div class="error_field_message">' . safeTags($messages["biography_error"]) . '</div>';
    }
    ?>


    <!-- CONTRACT ACCEPT -->
    <br>
    <label class="contract_accept">
      I agree with a contract
      <input type="checkbox" name="contract_accept">
    </label>
    <?php
    if (in_array("contract_accept_error", array_keys($messages))) {
      print '<div class="error_field_message">' . safeTags($messages["contract_accept_error"]) . '</div>';
    }
    ?>

    <!-- SUBMIT -->
    <div class="user-form__btn-wrap">
      <input class="btn_blue" type="submit" value="Send" name="send">

      <?php
      if (isset($_GET['do']) && $_GET['do'] == 'update') {
        print '<input class="btn_blue" type="button" value="Cansel" name="cansel" onclick="document.location.replace(`components/logout.php`)">';
      }
      ?>
    </div>

  </form>

</body>