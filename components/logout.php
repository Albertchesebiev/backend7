<?php
session_destroy();

// очищаем куки
foreach (array_keys($_COOKIE) as $cookieName) {
  if (strpos($cookieName, "_value")) {
    setcookie($cookieName, '', 1, '/backend7');
  }
}

header('Location: ../index.php');