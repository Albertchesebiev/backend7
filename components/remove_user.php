<?php
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
?>
  <!-- форма подтверждения -->
  <form action="" method="POST">
    <h3>Do you really want to delete selected users?</h3>
    <button type="submit" name="answer" value="yes">Yes</button>
    <button type="submit" name="answer" value="no">No</button>
  </form>
<?php
} else {
  if ($_POST['answer'] === 'yes') {
    include('db.php');
    // ID пользователей для удаления из БД
    $rm_users = [];
    foreach ($_COOKIE as $key => $value) {
      if (strstr($key, 'rm_user_')) {
        $rm_users[] = $value;
      }
    }
    if (empty($rm_users)) {
      // print 'empty';
      header('Location: ../views/admin.php');
    } else {
      // print '!empty';
      // очищаем куки
      foreach ($_COOKIE as $key => $value) {
        if (strstr($key, 'rm_user_')) {
          setcookie($key, '', 1, "/backend7/");
        }
      }
      // удаляем отмеченных юзеров из БД
      try {
        $query = "DELETE FROM user5 WHERE ";
        foreach ($rm_users as $id) {
          if ($id == end($rm_users)) {
            $query .= 'id = ' . intval($id);
          } else {
            $query .= 'id = ' . intval($id) . ' OR ';
          }
        }
        // print($query);
        $db = connectToDB();
        $stmt = $db->prepare($query);
        $stmt->execute();
      } catch (PDOException $e) {
        // print $e->getMessage();
        die();
      }
      header('Location: ../views/admin.php');
    }
  } else {
    // если "нет"
    header('Location: ../views/admin.php');
  }
}
