<head>
  <link rel="stylesheet" href="../css/main.css">
  <link rel="stylesheet" href="../css/admin.css">
</head>

<?php
/**
 * Задача 6. Реализовать вход администратора с использованием
 * HTTP-авторизации для просмотра и удаления результатов.
 **/


// TODO:
// сделать удаление через форму, а не куки
// <input name="security_token" type="hidden" value="<?= md5(time())?
// >">
// GET: insert token in DN
// POST: проверять token из формы и бд


// Пример HTTP-аутентификации.
// PHP хранит логин и пароль в суперглобальном массиве $_SERVER.
// Подробнее см. стр. 26 и 99 в учебном пособии Веб-программирование и веб-сервисы.
if (
  empty($_SERVER['PHP_AUTH_USER']) ||
  empty($_SERVER['PHP_AUTH_PW']) ||
  $_SERVER['PHP_AUTH_USER'] != 'admin' ||
  md5($_SERVER['PHP_AUTH_PW']) != md5('123')
) {
  header('HTTP/1.1 401 Unanthorized');
  header('WWW-Authenticate: Basic realm="admin"');
  print('<h1>401 Требуется авторизация</h1>');
  exit();
}

if (isset($_GET['do']) && $_GET['do'] === 'rm_users') {
  header('Location: ../components/remove_user.php');
}

header('Content-Type: text/html; charset=UTF-8');
include('../components/db.php');
include('../components/security.php');

try {
  $db = connectToDB();
  $stmt = $db->prepare("SELECT * FROM user5 ORDER BY id");
  $stmt->execute();
  $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
  // print_r($res);
} catch (PDOException $e) {
  // print $e->getMessage();
  die();
}

print('<h1>Admin Panel</h1>');

// вспомогательный массив для суперспособностей
$skills_labels = [
  'immortality' => 'Immortality',
  'idclip' => 'Passing Through Walls',
  'fly' => 'Fly'
];
?>

<div class="user-data_wrap">
  <?php foreach ($res as $user) { ?>
    <div class="user-data" name="<?= 'user_' . $user['id'] ?>">
      <!-- при нажатии на крестик скрываем карточку юзера и заносим его ID в куку, чтобы потом удалить из БД -->
      <div class="user-data_remove" name="<?= $user['id'] ?>" onclick="
        document.getElementsByName(`user_<?= $user['id'] ?>`)[0].style.display=`none`;
        document.cookie = 'rm_user_<?= $user['id'] ?> = <?= $user['id'] ?>; path=/backend7/';
      ">&#215;</div>
      <?php foreach ($user as $key => $value) {
        if ($key === 'pass_hash') {
          continue;
        }
        if ($key === 'id') {
          print '<h3>USER ID: ' . intval($value) . '</h3>';
          continue;
        }
        if (strstr($key, 'skill_')) {
          $key = substr($key, 6);
          $value = ($value == 1) ? 'Yes' : 'No';
        }
      ?>
        <div class="user-data_row">
          <?php
          if ($key === 'biography') {
            print '<b>' . unicode($key) . '</b>' . ': ' . safeTags($value);
          } else {
            print '<b>' . unicode($key) . '</b>' . ': ' . strip_tags($value);
          }
          ?>
        </div>
      <?php } ?>
    </div>
  <?php }

  print '<button class="btn_blue rm_users_btn" onclick="document.location.replace(`admin.php?do=rm_users`)">Confirm Update</button>';
  ?>
</div>